package com.glearning.hibernate.client;

import java.util.List;

import com.glearning.hibernate.dao.TeacherDAO;
import com.glearning.hibernate.model.Teacher;
import com.glearning.hibernate.model.TeacherDetails;

public class TeacherClient {

	/*
	 * This is the entry point for the application We create a teacher object. Call
	 * the DAO object Call the save method on the TeacherDAO
	 */
	public static void main(String[] args) {

		TeacherDAO teacherDAO = new TeacherDAO();

		saveTeachers(teacherDAO);
		List<Teacher> teachers = fetchAllTeachers(teacherDAO);
		int id = teachers.get(0).getId();
		Teacher firstTeacher = fetchTeacherById(teacherDAO, id);
		System.out.println("**********First teacher ***********");
		System.out.println(firstTeacher);
		System.out.println("Teacher Details :: " + firstTeacher.getTeacherDetails());
		System.out.println("**********First teacher ***********");
		/*
		 * List<Teacher> teachers = fetchAllTeachers(teacherDAO);
		 * 
		 * System.out.
		 * println("*******Initial list of teachers***************************");
		 * teachers.forEach(teacher -> System.out.println(teacher));
		 * System.out.println("**********************************");
		 * 
		 * int id = teachers.get(0).getId(); Teacher firstTeacher =
		 * fetchTeacherById(teacherDAO, id);
		 * 
		 * System.out.println("**********First teacher ***********");
		 * System.out.println(firstTeacher); System.out.println("Teacher Details :: "
		 * +firstTeacher.getTeacherDetails());
		 * System.out.println("**********First teacher ***********");
		 */

		/*
		 * //lets delete a teacher
		 * System.out.println("-----------------Delete method ------------------");
		 * System.out.println("Initial number of teachers :: "+ teachers.size());
		 * 
		 * deleteTeacher(teacherDAO, id);
		 * 
		 * System.out.println("Final number of teachers after deletion is :: "+
		 * fetchAllTeachers(teacherDAO).size());
		 * System.out.println("-----------------Delete method ------------------");
		 */ }

	private static void deleteTeacher(TeacherDAO teacherDAO, int id) {
		teacherDAO.deleteTeacherById(id);

	}

	private static Teacher fetchTeacherById(TeacherDAO teacherDAO, int id) {
		Teacher teacher = teacherDAO.fetchTeacherById(id);
		return teacher;
	}

	private static List<Teacher> fetchAllTeachers(TeacherDAO teacherDAO) {
		List<Teacher> teachers = teacherDAO.fetchAllTeachers();
		return teachers;

	}

	private static void saveTeachers(TeacherDAO teacherDAO) {
		// create a teacher object
		Teacher harish = new Teacher("Harish", "harish@gmail.com", "English");
		TeacherDetails harishDetails = new TeacherDetails("Pune", "Playing Guitar");
		harish.setTeacherDetails(harishDetails);

		Teacher vinay = new Teacher("Vinay", "vinay@gmail.com", "English");
		TeacherDetails vinayDetails = new TeacherDetails("Mumbai", "Trekking");
		vinayDetails.setTeacher(vinay);
		vinay.setTeacherDetails(vinayDetails);

		Teacher rahul = new Teacher("Rahul", "rahul@gmail.com", "English");
		TeacherDetails rahulDetails = new TeacherDetails("Chennai", "Playing Golf");
		rahul.setTeacherDetails(rahulDetails);
		rahulDetails.setTeacher(rahul);

		Teacher anirban = new Teacher("Anirban", "anirban@gmail.com", "English");
		TeacherDetails anirbanDetails = new TeacherDetails("Kolkata", "Singing");
		anirban.setTeacherDetails(anirbanDetails);
		anirbanDetails.setTeacher(anirban);

		Teacher vimal = new Teacher("Vimal", "vimal@gmail.com", "English");
		TeacherDetails vimalDetails = new TeacherDetails("Delhi", "Singing");
		vimal.setTeacherDetails(vimalDetails);
		vimalDetails.setTeacher(vimal);

		teacherDAO.save(vinay);
		teacherDAO.save(harish);
		teacherDAO.save(rahul);
		teacherDAO.save(anirban);
		teacherDAO.save(vimal);
	}

}
