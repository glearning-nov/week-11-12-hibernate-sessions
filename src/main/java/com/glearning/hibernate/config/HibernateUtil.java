package com.glearning.hibernate.config;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	
	private static final SessionFactory sessionFactory = buildSessionFactory();



	//which returns a session factory by loading the hibernate-configuration file
	private static SessionFactory buildSessionFactory() {
		/*
		 * Load the hibernate-cfg.xml and create a SessionFactory
		 */
		
		Configuration configuration = new Configuration().configure("hibernate-cfg.xml");
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		return sessionFactory;
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory ;
	}

}
