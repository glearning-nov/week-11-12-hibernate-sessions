package com.glearning.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.glearning.hibernate.config.HibernateUtil;
import com.glearning.hibernate.model.Teacher;

public class TeacherDAO {
	/**
	 * This method will save the teacher to the table
	 * 
	 * @param teacher
	 * @return
	 */
	public Teacher save(Teacher teacher) {
		//save the input to the database and return the saved Teacher
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Serializable id = session.save(teacher);
		transaction.commit();
		session.close();
		return teacher;
	}
	
	public List<Teacher> fetchAllTeachers(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		List<Teacher> teachers = session.createQuery("FROM Teacher", Teacher.class).list();
		transaction.commit();
		session.close();
		return teachers;
	}
	
	public Teacher fetchTeacherById(int teacherId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Teacher teacher = session.get(Teacher.class, teacherId);
		transaction.commit();
		session.close();
		return teacher;
	}
	
	public void deleteTeacherById(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		
		Teacher teacher = session.get(Teacher.class, id);
		if(teacher != null) {
			session.delete(teacher);
		}
		transaction.commit();
		session.close();
	}

}
